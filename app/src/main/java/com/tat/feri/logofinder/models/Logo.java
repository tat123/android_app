package com.tat.feri.logofinder.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tdx on 17/05/2018.
 */

public class Logo extends DefaultModel {
    public static final String identifier = "LogoID";

    private int id;
    private String name;
    private String asset;
    private String clue;
    private double lat;
    private double lng;
    private double distance;

    private Question question;

    public Logo(int id, String name, String asset, String clue, double lat, double lng, Question question) {
        this.id = id;
        this.name = name;
        this.asset = asset;
        this.clue = clue;
        this.lat = lat;
        this.lng = lng;
        this.question = question;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getClue() {
        return clue;
    }

    public void setClue(String clue) {
        this.clue = clue;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public static Logo find(int ID) {
        return Logo.all().get(ID);
    }

    public Question getQuestion() {
        return question;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public static List<Logo> all() {
        try {
            String response = DefaultModel.BackgroundTaskGet().execute(Logo.class).get();
            Type listType = new TypeToken<List<Logo>>(){}.getType();

            List<Logo> list = new Gson().fromJson(response, listType);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Arrays.asList(
            new Logo(1, "Mercator", "https://upload.wikimedia.org/wikipedia/sr/b/ba/Mercator_Slovenia.jpg", "Najdi merkator?", 46.563494,15.6345356, Question.find(1)),
            new Logo(2, "Spar", "https://www.spar.co.uk/media/5145/12885808_763137237121813_8477488662282615668_o.jpg", "Najdi spar?", 46.5634914,15.6345356, Question.find(1)),
            new Logo(3, "H&M", "https://business.mb.com.ph/wp-content/uploads/2017/03/photo.jpg", "Najdi H&M?", 46.5569347,15.6485213, Question.find(1)),
            new Logo(4, "Deichman", "https://s3-media2.fl.yelpcdn.com/bphoto/gXFR7WweeteuboTuFB_2lg/ls.jpg", "Najdi Deichman?",46.5569334,15.6332004, Question.find(1))
        );
    }
}
