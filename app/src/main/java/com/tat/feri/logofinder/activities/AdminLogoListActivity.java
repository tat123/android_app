package com.tat.feri.logofinder.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.tat.feri.logofinder.AdapterLogos;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.models.Logo;

import java.util.ArrayList;
import java.util.List;

public class AdminLogoListActivity extends AppCompatActivity {

    ApplicationMy app;
    RecyclerView recyclerView;
    AdapterLogos adapterLogos;

    List<Logo> logos = new ArrayList();

    Button btnAdd;

    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_logo_list);

        app = (ApplicationMy) this.getApplication();

        recyclerView = (RecyclerView) findViewById(R.id.list_view);
        layoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(layoutManager);
        adapterLogos = new AdapterLogos(Logo.all(), AdminLogoListActivity.this);
        recyclerView.setAdapter(adapterLogos);
        adapterLogos.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        super.onResume();
        adapterLogos.notifyDataSetChanged();
    }
}
