package com.tat.feri.logofinder.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.models.Logo;
import com.tat.feri.logofinder.models.Question;

import java.lang.reflect.Type;

public class Quiz2Activity extends AppCompatActivity {


    private Question question;

    private TextView questionText;
    private Button answer1, answer2, answer3, answer4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz2);

        Bundle extras = getIntent().getExtras();

        Type listType = new TypeToken<Logo>(){}.getType();

        Logo logo = new Gson().fromJson(extras.getString("logo"), listType);

        question = logo.getQuestion();

        questionText = (TextView)findViewById(R.id.question);
        questionText.setText(question.getContent());

        answer1 = (Button)findViewById(R.id.answer1);
        answer1.setText(question.getAnswers()[0].getContent());
        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz2Activity.this, QuizResultActivity.class);
                if (question.getAnswers()[0].getCorrect() == 1) {
                    intent.putExtra("status", "Correct answer");
                } else {
                    intent.putExtra("status", "Incorrect answer");
                }
                startActivity(intent);
            }
        });
        answer2 = (Button)findViewById(R.id.answer2);
        answer2.setText(question.getAnswers()[1].getContent());
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz2Activity.this, QuizResultActivity.class);
                if (question.getAnswers()[1].getCorrect() == 1) {
                    intent.putExtra("status", "Correct answer");
                } else {
                    intent.putExtra("status", "Incorrect answer");
                }
                startActivity(intent);
            }
        });
        answer3 = (Button)findViewById(R.id.answer3);
        answer3.setText(question.getAnswers()[2].getContent());
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz2Activity.this, QuizResultActivity.class);
                if (question.getAnswers()[2].getCorrect() == 1) {
                    intent.putExtra("status", "Correct answer");
                } else {
                    intent.putExtra("status", "Incorrect answer");
                }
                startActivity(intent);
            }
        });
        answer4 = (Button)findViewById(R.id.answer4);
        answer4.setText(question.getAnswers()[3].getContent());
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz2Activity.this, QuizResultActivity.class);
                if (question.getAnswers()[3].getCorrect() == 1) {
                    intent.putExtra("status", "Correct answer");
                } else {
                    intent.putExtra("status", "Incorrect answer");
                }
                startActivity(intent);
            }
        });
    }
}
