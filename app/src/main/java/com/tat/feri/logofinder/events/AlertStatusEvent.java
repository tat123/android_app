package com.tat.feri.logofinder.events;

/**
 * Created by tdx on 06/06/2018.
 */

public class AlertStatusEvent {
    private boolean isCanceled;

    public AlertStatusEvent(boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    public boolean isCanceled() {
        return isCanceled;
    }
}
