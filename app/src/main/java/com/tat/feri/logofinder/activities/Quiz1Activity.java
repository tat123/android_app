package com.tat.feri.logofinder.activities;

import android.*;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.HttpUtils;
//import com.tat.feri.logofinder.ImageUtils;
import com.tat.feri.logofinder.MultipartUtility;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.models.DefaultModel;
import com.tat.feri.logofinder.models.Logo;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class Quiz1Activity extends AppCompatActivity {

    ApplicationMy app;

    private Logo logo;

    private TextView clue, status;
    private Button takePhoto;

    private Bitmap image;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz1);

        app = (ApplicationMy)getApplication();

        Bundle extras = getIntent().getExtras();

        Type listType = new TypeToken<Logo>(){}.getType();

        logo = new Gson().fromJson(extras.getString("logo"), listType);

        clue = (TextView)findViewById(R.id.clue);
        clue.setText(logo.getClue());

        takePhoto = (Button)findViewById(R.id.take_picture);
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakePicture();
            }
        });

        status = (TextView)findViewById(R.id.status);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            takePhoto.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] { android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takePhoto.setEnabled(true);
            }
        }
    }

    void TakePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            image = (Bitmap)extras.get("data");

            takePhoto.setVisibility(View.GONE);

            processImage();
        }
    }

private void processImage() {

StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
StrictMode.setThreadPolicy(policy);



try {


    File filesDir = this.getBaseContext().getFilesDir();
    File imageFile = new File(filesDir,  "temp.jpg");

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    image.compress(Bitmap.CompressFormat.JPEG, 100, bos);
    byte[] bitmapdata = bos.toByteArray();
    ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);

    // Connect to the web server endpoint
    URL serverUrl =
            new URL(HttpUtils.url("logos/Compare"));
    HttpURLConnection urlConnection = (HttpURLConnection) serverUrl.openConnection();

    String boundaryString = "----SomeRandomText";

    // Indicate that we want to write to the HTTP request body
    urlConnection.setDoOutput(true);
    urlConnection.setRequestMethod("POST");
    urlConnection.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundaryString);

    OutputStream outputStreamToRequestBody = urlConnection.getOutputStream();
    BufferedWriter httpRequestBodyWriter =
            new BufferedWriter(new OutputStreamWriter(outputStreamToRequestBody));

    // Include value from the myFileDescription text area in the post data
    httpRequestBodyWriter.write("\n\n--" + boundaryString + "\n");
    httpRequestBodyWriter.write("Content-Disposition: form-data; name=\"name\"");
    httpRequestBodyWriter.write("\n\n");
    httpRequestBodyWriter.write("9");

    // Include the section to describe the file
    httpRequestBodyWriter.write("\n--" + boundaryString + "\n");
    httpRequestBodyWriter.write("Content-Disposition: form-data;"
            + "name=\"image\";"
            + "filename=\"image.jpg\""
            + "\nContent-Type: text/plain\n\n");
    httpRequestBodyWriter.flush();

    // Write the actual file contents
    FileInputStream inputStreamToLogFile = new FileInputStream(imageFile);

    outputStreamToRequestBody.write(bitmapdata, 0, bitmapdata.length);

    outputStreamToRequestBody.flush();

    // Mark the end of the multipart http request
    httpRequestBodyWriter.write("\n--" + boundaryString + "--\n");
    httpRequestBodyWriter.flush();

    // Close the streams
    outputStreamToRequestBody.close();
    httpRequestBodyWriter.close();

    urlConnection.connect();

    int code = urlConnection.getResponseCode();

    Log.e("LOGO", urlConnection.getResponseMessage());
    if (200 <= code && code <= 299) {
        String response = DefaultModel.readInputStreamToString(urlConnection);
        Log.e("LOGO", response.toString());

        // is correct
        Intent intent = new Intent(Quiz1Activity.this, Quiz2Activity.class);
        intent.putExtra("logo", new Gson().toJson(logo));
        startActivity(intent);
    } else {
        // is incorrect
        takePhoto.setVisibility(View.VISIBLE);

        status.setText("Image is not match. Try again!");
        status.setVisibility(View.VISIBLE);
    }
} catch (Exception e) {
    e.printStackTrace();
    // is incorrect
    takePhoto.setVisibility(View.VISIBLE);

    status.setText("Image is not match. Try again!");
    status.setVisibility(View.VISIBLE);
}

}



}
