package com.tat.feri.logofinder.models;

import android.os.AsyncTask;
import android.util.Log;

import com.tat.feri.logofinder.HttpUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by tdx on 04/06/2018.
 */

public class DefaultModel {

    public static String readInputStreamToString(HttpURLConnection connection) {
        String result = null;
        StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        }
        catch (Exception e) {
            Log.i("NETWORK", "Error reading InputStream");
            result = null;
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    Log.i("NETWORK", "Error closing InputStream");
                }
            }
        }

        return result;
    }

    protected static BackgroundTask BackgroundTaskGet() {
        return new BackgroundTask();
    }

    public static class BackgroundTask extends AsyncTask<Class, String, String> {
        @Override
        protected String doInBackground(Class... cls) {
            try {
                URL url = new URL(HttpUtils.url("logos/get"));
                HttpURLConnection connection = (HttpURLConnection)
                        url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int code = connection.getResponseCode();

                if (200 <= code && code <= 299) {
                    return DefaultModel.readInputStreamToString(connection);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

//    public static <T> T find(int ID) {
//        return DefaultModel.<T>all().get(ID);
//    }
//
//    public static <T> List<T> all() {
//        Log.e("JSON", "JSON RULE");
//
//
//        HttpUtils.get("logos/get", new RequestParams(), new JsonHttpResponseHandler() {
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
//                super.onSuccess(statusCode, headers, response);
//                Log.e("JSON", response.toString());
//                Type listType = new TypeToken<List<T>>(){}.getType();
//                Gson gson = new Gson();
//                List<T> object = new Gson().fromJson(response.toString(), listType);
//                Log.e("JSON", object.toString());
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                super.onFailure(statusCode, headers, throwable, errorResponse);
//            }
//        });
//
//        return new ArrayList<>();
//    }
}
