package com.tat.feri.logofinder.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.HttpUtils;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.events.LocationCheckEvent;
import com.tat.feri.logofinder.events.LocationDetected;
import com.tat.feri.logofinder.models.DefaultModel;
import com.tat.feri.logofinder.models.Logo;
import com.tat.feri.logofinder.services.GPSTracker;
import com.tat.feri.logofinder.services.LogoTracker;
import com.tat.feri.logofinder.views.CompasView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class CompasActivity extends AppCompatActivity implements SensorEventListener {

    ApplicationMy app;

    private Toolbar toolbar;

    // define the display assembly compass picture
    private CompasView compasView;
    private TextView distanceTxt;

    // record the compass picture angle turned
    private float currentDegree = 0f;

    // device sensor manager
    private SensorManager mSensorManager;

    private Location myLocation;
    private Location target;

    private Logo logo;
    private Location mLocation;

    private ConstraintLayout layoutMain;
    private LottieAnimationView animationViewLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compas);

        app = (ApplicationMy)getApplication();
        if (!app.isLoggedIn()) {
            Intent intent = new Intent(this.getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        layoutMain = (ConstraintLayout) findViewById(R.id.layoutMain);
        animationViewLoading = (LottieAnimationView)findViewById(R.id.animationViewLoading);

        startLoading();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Logo Hunter");
        toolbar.inflateMenu(R.menu.actionbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent;
                switch (item.getItemId()) {
                    case R.id.logout:
                        app.signOut();
                        intent = new Intent(CompasActivity.this, LoginActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.settings:
                        intent = new Intent(CompasActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        return true;
                }

                return false;
            }
        });

        //
        compasView = (CompasView)findViewById(R.id.compas_view);
        distanceTxt = (TextView)findViewById(R.id.distanceTxt);

        myLocation = new Location("ME");

        target = new Location("MERKATOR");

        // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        toolbar.bringToFront();
    }


    @Override
    protected void onResume() {
        super.onResume();

        app = (ApplicationMy)getApplication();
        if (!app.isLoggedIn()) {
            Intent intent = new Intent(this.getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        startService(new Intent(this, GPSTracker.class));//start service
        startService(new Intent(this, LogoTracker.class));//start service

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);

    }

    @Override
    protected void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService(new Intent(this, GPSTracker.class));
        stopService(new Intent(this, LogoTracker.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                app.signOut();
                Intent intent = new Intent(CompasActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Log.e("APP", "COMPAS CHANGE");

        if (mLocation != null && logo != null) {

            endLoading();

            target.setLatitude(logo.getLat());
            target.setLongitude(logo.getLng());

            // get the angle around the z-axis rotated
            float degree = Math.round(event.values[0]);

            float bearing = mLocation.bearingTo(target);
            degree = (bearing - degree) * -1;
            degree = normalizeDegree(degree);

            int distance = (int)Math.round(mLocation.distanceTo(target));
            distanceTxt.setText(distance + "m");

//            if (distance < 500) {
//                Toast toast = Toast.makeText(CompasActivity.this, "Logo je blizu. Slikaj ga!", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0 ,200);
//                View view = toast.getView();
//
////Gets the actual oval background of the Toast then sets the colour filter
//                view.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
//
////Gets the TextView from the Toast so it can be editted
//                TextView text = view.findViewById(android.R.id.message);
//                text.setTextColor(Color.WHITE);
//                toast.show();
//
//                Intent intent = new Intent(CompasActivity.this, CameraActivity.class);
//                startActivity(intent);
//            }


            // create a rotation animation (reverse turn degree degrees)
            RotateAnimation ra = new RotateAnimation(
                    currentDegree,
                    -degree,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f);

            // how long the animation will take place
            ra.setDuration(210);

            // set the animation after the end of the reservation status
            ra.setFillAfter(true);

            // Start the animation
            compasView.startAnimation(ra);
            currentDegree = -degree;
        }

    }

    private float normalizeDegree(float value) {
        if (value >= 0.0f && value <= 180.0f) {
            return value;
        } else {
            return 180 + (180 + value);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not in use
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationDetected event) {
        if (mLocation == null) {
            mLocation = event.getLocation();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationCheckEvent event) {
        if (event.hasLogo()) {
            final Logo logo = event.getLogo();
            if (this.logo == null) {
                this.logo = logo;
            }
        }
    }

    private void startLoading() {
        animationViewLoading.setRepeatCount(LottieDrawable.INFINITE);
        animationViewLoading.resumeAnimation();
        animationViewLoading.setVisibility(View.VISIBLE);
        layoutMain.setVisibility(View.GONE);
    }

    private void endLoading() {
        animationViewLoading.setVisibility(View.GONE);
        layoutMain.setVisibility(View.VISIBLE);
    }
}
