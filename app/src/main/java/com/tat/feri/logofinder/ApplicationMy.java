package com.tat.feri.logofinder;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.FileInputStream;

/**
 * Created by Matej on 13. 03. 2018.
 */

public class ApplicationMy extends Application {

    public FirebaseAuth mAuth;

    FirebaseUser user = null;

    public ApplicationMy() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();


    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void signOut() {
        mAuth.signOut();
        LoginManager.getInstance().logOut();
        setUser(null);
    }

    public void redirect(Activity activity, Class<?> cls) {
        Intent intent = new Intent(activity, cls);
        startActivity(intent);
    }

}
