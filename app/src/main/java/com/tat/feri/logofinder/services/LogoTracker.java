package com.tat.feri.logofinder.services;

/**
 * Created by tdx on 06/06/2018.
 */

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.HttpUtils;
import com.tat.feri.logofinder.events.AlertStatusEvent;
import com.tat.feri.logofinder.events.LocationCheckEvent;
import com.tat.feri.logofinder.events.LocationDetected;
import com.tat.feri.logofinder.models.DefaultModel;
import com.tat.feri.logofinder.models.Logo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class LogoTracker extends Service  {

    private static final String TAG = LogoTracker.class.getSimpleName();

    private Location mLocation;

    private ApplicationMy app;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);


        return START_STICKY;
    }

    private class CheckLocationCall extends AsyncTask<String, String, Logo> {
        @Override
        protected Logo doInBackground(String... strings) {
            Log.e("LOGO", "CHECKING FOR LOGO");
//            Log.e("LOGO", mLocation.toString());
            if (mLocation == null) return null;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            try {
                URL url = new URL(HttpUtils.url("logos/getDistance"));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");

                Log.e("LOGO", "CHECKING AT: " +  mLocation.getLongitude() + ", " + mLocation.getLatitude());

                String myData = "lat=" + mLocation.getLatitude() + "&lng=" + mLocation.getLongitude() + "&userId=" + app.mAuth.getUid();
                connection.setDoOutput(true);
                connection.getOutputStream().write(myData.getBytes());

                connection.connect();

                int code = connection.getResponseCode();

                Log.e("LOGO", connection.getResponseMessage());
                Log.e("LOGO", connection.getInputStream().toString());
                if (200 <= code && code <= 299) {
                    String response = DefaultModel.readInputStreamToString(connection);
                    try {
                        Type listType = new TypeToken<Logo>(){}.getType();
                        return new Gson().fromJson(response, listType);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("LOGO",  "PARSE ERROR");
                    }
                } else {
                    Log.e("LOGO",  "STATUS ERROR");
                }
            } catch (Exception e) {
                Log.e("LOGO",  "HTTP ERROR");
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Logo logo) {
            super.onPostExecute(logo);

//            Log.e("LOGO", "LOCATION NOT BERABY");

            if (logo != null) {
                Log.e(TAG, "LOCATION NEARBY");
                EventBus.getDefault().post(new LocationCheckEvent(logo));
                new CheckLocationCall().execute();
            } else {
                new CheckLocationCall().execute();
            }
        }
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");

        app = (ApplicationMy)getApplication();

        EventBus.getDefault().register(this);

        new CheckLocationCall().execute();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AlertStatusEvent event) {
        if (event.isCanceled()) {
            new CheckLocationCall().execute();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationDetected event) {
        Log.e("LOGO", "LOCATION SET");
        mLocation = event.getLocation();
    }
}