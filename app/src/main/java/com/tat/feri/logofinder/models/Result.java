package com.tat.feri.logofinder.models;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tdx on 17/05/2018.
 */

public class Result {

    private int ID;
    private Logo logo;
    private String question;
    private int time;
    private int stage;

    public Result(int ID, Logo logo, String question, int time, int stage) {
        this.ID = ID;
        this.logo = logo;
        this.question = question;
        this.time = time;
        this.stage = stage;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public static List<Result> all() {
        return Arrays.asList(
            new Result(1, Logo.all().get(1), "Where is...", 20,2),
            new Result(2, Logo.all().get(1), "Where is...", 20,0),
            new Result(3, Logo.all().get(2), "Where is...", 20,1),
            new Result(4, Logo.all().get(3), "Where is...", 20,2)
        );
    }
}
