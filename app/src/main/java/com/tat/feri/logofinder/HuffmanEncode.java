package com.tat.feri.logofinder;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by tdx on 09/01/2019.
 */

public class HuffmanEncode {
    class Node {
        int symbol;
        int weight;
        Node left;
        Node right;

        public Node(int symbol, int weight) {
            this(symbol, weight, null, null);
        }

        public Node(int symbol, int weight, Node left, Node right) {
            this.symbol = symbol;
            this.weight = weight;
            this.left   = left;
            this.right  = right;
        }
    }


    private class SortByWeight implements Comparator<Node> {
        public int compare(Node a, Node b) {
            if ( a.weight < b.weight ) return 1;
            else if ( a.weight == b.weight ) return 0;
            else return -1;
        }
    }

    private List<Integer> input;
    private ByteArrayOutputStream os;

    public HuffmanEncode() {
//        this.input = input;
        this.os = new ByteArrayOutputStream();
    }

    public HuffmanEncode(List<Integer> input) {
        this.input = input;
        this.os = new ByteArrayOutputStream();
    }

    public void encode() {
        Node[] freqTbl = createFreqTbl();
        Node root = createTree(freqTbl);
        Map<Integer, List<Boolean>> codes =  createCodeTbl(root);


//        Iterator<Map.Entry<Integer, List<Boolean>>> iter = codes.entrySet().iterator();
//        while (iter.hasNext()) {
//            Map.Entry<Integer, List<Boolean>> entry = iter.next();
//            String bool = "";
//            for (boolean b : entry.getValue()) {
//                bool += b ? "1" : "0";
//            }
//            Log.e("CODES", entry.getKey() + "=" + bool);
//        }

        BinStreamWriter bw = new BinStreamWriter(os);

        // create huffman header
        bw.writeInt(freqTbl.length); // write symbols count
        Log.e("SYMBOLS", freqTbl.length + " INPUT");

        int indx = 0;
        for (Node n : freqTbl) {
            bw.writeInt(n.symbol);
            bw.writeInt(n.weight);
            Log.e("SYMBS", indx++ + "=" + n.symbol + "::" + n.weight);
        }
        Log.e("HEADER", "END HEADER");


        indx = 0;
        // create huffman body
        for (int d : input) {
//            String code = "";
            for (boolean b : codes.get(d)) {
                bw.writeBit(b);
//                code += b ? "1" : "0";
//                if (indx < 20)
//                    Log.e("BOOL", "INT" + indx++ + "=" + (b ? 1 : 0) );
            }
//            if (d != 0 && indx < 10)
//                Log.e("OUTPUT", indx++ + "=" + d + "=" + code  + " IN");
        }
//        Log.e("PARSE", "BODY FINITO");
//
    }

    public void decode(BufferedInputStream stream) {
        BinStreamReader br = new BinStreamReader(stream);

        List<Node> freqTbl = new LinkedList<>();
        int symbolCount = br.readInt();
        Log.e("SYMBOLS", symbolCount + " FILE");
        for (int i = 0; i < symbolCount; i++) {
            int symbol = br.readInt();
            int weight = br.readInt();

            freqTbl.add(new Node(symbol, weight));
            Log.e("SYMBS", i + "=" + symbol + "::" + weight);
        }

//        freqTbl = bubbleSort(freqTbl);

        Node[] freqArr = freqTbl.toArray(new Node[freqTbl.size()]);
        Arrays.sort(freqArr, new SortByWeight());

        List<Integer> list = new LinkedList<>();
        Node tree = createTree(freqTbl.toArray(new Node[freqTbl.size()]));
        Node current = tree;
        int indx = 0;
        while (!br.isEmpty()) {
            boolean bit = br.readBoolean();
//            if (indx < 20)
//                Log.e("BOOL", "OUT" + indx++ + "=" + (bit ? 1 : 0) );
            if (current.left == null && current.right == null) {
                list.add(current.symbol);
//                if (current.symbol != 0 && indx < 10)
//                    Log.e("OUTPUT", indx++ +"="+ current.symbol + " sad");
                current = tree;
            }

            if (bit)
                current = current.right;
            else
                current = current.left;
        }


        Log.e("RESULT", "FINAL" + list.get(0));
        Log.e("RESULT", "FINAL" + list.get(1));
        Log.e("RESULT", "FINAL" + list.get(2));
        Log.e("RESULT", "FINAL" + list.get(3));
        Log.e("RESULT", "FINAL" + list.get(4));
        Log.e("RESULT", "FINAL" + list.get(5));
        Log.e("RESULT", "FINAL" + list.get(6));
        Log.e("RESULT", "FINAL" + list.get(7));


    }

    private Node[] createFreqTbl() {
        List<Node> freq = new LinkedList<>();

        for (Integer d : input) {
            boolean exists = false;

            for (Node n : freq) {
                if (n.symbol == d) {
                    n.weight++;
                    exists = true;
                }
            }

            if (!exists) {
                Node n = new Node(d, 1);
                freq.add(n);
            }
        }


//        freq = bubbleSort(freq);
//        Node[] freqArr = freqTbl.toArray(new Node[freqTbl.size()]);

        Node[] freqArr = freq.toArray(new Node[freq.size()]);
        Arrays.sort(freqArr, new SortByWeight());

        return freq.toArray(new Node[freq.size()]);
    }

    private Node createTree(Node[] freqTbl) {
//        List<Node> list = Arrays.asList(freqTbl);
        List<Node> list = new LinkedList(Arrays.asList(freqTbl));

        int i = 0;
        for (Node n : list) {
            Log.e("NODE", i++ + ". " + n.symbol + " :: " + n.weight);
        }

        while (list.size() > 1) {
            // get and delete LAST element
            Node n_right = list.get(list.size() - 1);
            list.remove(n_right);
//                list.remove(list.size() - 1);

            // get and delete LAST element
            Node n_left = list.get(list.size() - 1);
            list.remove(list.size() - 1);

            // create new node
            Node n = new Node(5000, n_right.weight + n_left.weight, n_left, n_right);

            // add node to frequency table
            list.add(n);
//            list = bubbleSort(list);

            Node[] array = list.toArray(new Node[list.size()]);
            Arrays.sort(array, new SortByWeight());
            list = new LinkedList(Arrays.asList(array));
        }

        Node root = list.get(list.size() - 1);

        return root;
    }

    private Map<Integer, List<Boolean>> createCodeTbl(Node n) {
        Map<Integer, List<Boolean>> codes = new HashMap();
        List<Boolean> code = new ArrayList();

        createCodeTableUtil(n, codes, code);

        return codes;
    }

    private void createCodeTableUtil(Node n, Map<Integer, List<Boolean>> codes, List<Boolean> code) {

        if (n.left == null && n.right == null) {
            codes.put(n.symbol, code);
            return;
        }

        // add 0 to code // LEFT SIDE

        List<Boolean> code_l = new ArrayList<>(code);
        code_l.add(false);

        // search LEFT
        createCodeTableUtil(n.left, codes, code_l);

        // add 1 to code // RIGHT SIDE

        List<Boolean> code_r = new ArrayList<>(code);
        code_r.add(true);

        // search RIGHT
        createCodeTableUtil(n.right, codes, code_r);
    }

    public ByteArrayOutputStream getStream() {
        return this.os;
    }
}


