package com.tat.feri.logofinder.events;

import com.tat.feri.logofinder.models.Logo;

/**
 * Created by tdx on 06/06/2018.
 */

public class LocationCheckEvent {
    private Logo logo;

    public LocationCheckEvent(Logo logo) {
        this.logo = logo;
    }

    public boolean hasLogo() {
        return logo != null;
    }

    public Logo getLogo() {
        return logo;
    }
}
