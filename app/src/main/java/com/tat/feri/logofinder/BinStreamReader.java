package com.tat.feri.logofinder;

/**
 * Created by tdx on 10/01/2019.
 */

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.NoSuchElementException;

public class BinStreamReader
{
    private static final int EOF = -1;   // end of file

    private BufferedInputStream in;

    private int buffer;                  // one character buffer
    private int n;                       // number of bits left in buffer

    /** Creates a OutputStreamBitWriter from an OutputStream */
    public BinStreamReader(BufferedInputStream s)
    {
        in = s;
        fillBuffer();
    }

    /**
     * Create a OutputStreamBitWriter
     *
     * @param filename
     *            an absolute or relative filename
     * @exception FileNotFoundException
     *                thrown if the file is not writable
     */
    public BinStreamReader(String filename)
    {
        try
        {
            in = new BufferedInputStream(new FileInputStream(filename));
        }
        catch(FileNotFoundException e)
        {
            System.out.println(filename + " does not exist");
        }
        fillBuffer();

    }


    public boolean readBoolean ()
    {
        if (isEmpty()) throw new NoSuchElementException("Reading from empty input stream");
        n--;
        boolean bit = ((buffer >> n) & 1) == 1;
        if (n == 0) fillBuffer();
        return bit;

    }

    public char readChar() {
        if (isEmpty()) throw new NoSuchElementException("Reading from empty input stream");

        // special case when aligned byte
        if (n == 8) {
            int x = buffer;
            fillBuffer();
            return (char) (x & 0xff);
        }

        // combine last N bits of current buffer with first 8-N bits of new buffer
        int x = buffer;
        x <<= (8 - n);
        int oldN = n;
        fillBuffer();
        if (isEmpty()) throw new NoSuchElementException("Reading from empty input stream");
        n = oldN;
        x |= (buffer >>> n);
        return (char) (x & 0xff);
        // the above code doesn't quite work for the last character if N = 8
        // because buffer will be -1
    }


    public int readInt() {
        int x = 0;
        for (int i = 0; i < 4; i++) {
            char c = readChar();
            x <<= 8;
            x |= c;
        }
        return x;
    }

    public boolean isEmpty() {
        return buffer == EOF;
    }

    private void fillBuffer() {
        try {
            buffer = in.read();
            n = 8;
        }
        catch (IOException e) {
            System.err.println("EOF");
            buffer = EOF;
            n = -1;
        }
    }
}