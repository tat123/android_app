package com.tat.feri.logofinder.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.HttpUtils;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.events.AlertStatusEvent;
import com.tat.feri.logofinder.events.LocationCheckEvent;
import com.tat.feri.logofinder.events.LocationDetected;
import com.tat.feri.logofinder.models.DefaultModel;
import com.tat.feri.logofinder.models.Logo;
import com.tat.feri.logofinder.models.Result;
import com.tat.feri.logofinder.services.GPSTracker;
import com.tat.feri.logofinder.services.LogoTracker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    ApplicationMy app;

    private GoogleMap mMap;

    private Toolbar toolbar;

    private boolean mapCentered = true;

    private LinearLayout layoutMain;
    private LottieAnimationView animationViewLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        app = (ApplicationMy)getApplication();
        if (!app.isLoggedIn()) {
            Intent intent = new Intent(this.getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        layoutMain = (LinearLayout)findViewById(R.id.layoutMain);
        animationViewLoading = (LottieAnimationView)findViewById(R.id.animationViewLoading);

        startLoading();

        new GetResults().execute();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Logo Hunter");
        toolbar.inflateMenu(R.menu.actionbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent;
                switch (item.getItemId()) {
                case R.id.logout:
                    app.signOut();
                    intent = new Intent(MapsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.settings:
                    intent = new Intent(MapsActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        app = (ApplicationMy)getApplication();
        if (!app.isLoggedIn()) {
            Intent intent = new Intent(this.getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        mapCentered = false;

        startService(new Intent(this, GPSTracker.class));//start service
        startService(new Intent(this, LogoTracker.class));//start service
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService(new Intent(this, GPSTracker.class));
        stopService(new Intent(this, LogoTracker.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationCheckEvent event) {
        if (event.hasLogo()) {
            final Logo logo = event.getLogo();
            Log.e("DISTANCE", logo.getDistance() + " sad");
            if (logo.getDistance() < 0.1) {
                Intent intent = new Intent(MapsActivity.this, CompasActivity.class);
                startActivity(intent);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationDetected event) {
        Log.e("LOGO", "LOCATION DETECTED" + event.getLocation().getLatitude());
//        EventBus.getDefault().post(new AlertStatusEvent(true));

        if (!mapCentered) {
            Location location = event.getLocation();

            // Add a marker in Sydney and move the camera
            LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    latlng,
                    16.0f
            ));

            mapCentered = true;
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            mMap.setMyLocationEnabled(true);
        else
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION }, 0);


//        addResults();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                finish();
                startActivity(getIntent());
            }
        }
    }

    private void addResults(List<Logo> logos) {
        for (Logo logo : logos) {
            Marker marker = mMap.addMarker(
                new MarkerOptions()
                        .position(new LatLng(logo.getLat(), logo.getLng()))
                        .title(logo.getName())
            );
            marker.showInfoWindow();
        }
    }

    class GetResults extends AsyncTask<String, String, List<Logo>> {
        @Override
        protected List<Logo> doInBackground(String... strings) {
            Log.e("LOGO", "POSTING LOGO RESULT");

            try {
                URL url = new URL(HttpUtils.url("logos/getResults"));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");

                String myData = "userId=" + app.mAuth.getUid();
                Log.e("POST_DATA", myData);
                connection.setDoOutput(true);
                connection.getOutputStream().write(myData.getBytes());

                connection.connect();

                int code = connection.getResponseCode();

                Log.e("LOGO", connection.getResponseMessage());
                Log.e("LOGO", connection.getInputStream().toString());
                if (200 <= code && code <= 299) {
                    String response = DefaultModel.readInputStreamToString(connection);
                    try {
                        Type listType = new TypeToken<List<Logo>>(){}.getType();
                        return new Gson().fromJson(response, listType);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("LOGO",  "PARSE ERROR");
                    }
                } else {
                    Log.e("LOGO",  "STATUS ERROR");
                }
            } catch (Exception e) {
                Log.e("LOGO",  "HTTP ERROR");
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(List<Logo> logos) {
            super.onPostExecute(logos);
            addResults(logos);
            endLoading();
            Log.e("LOGOS", logos.size() + "asd");
        }
    }

    private void startLoading() {
        animationViewLoading.setRepeatCount(LottieDrawable.INFINITE);
        animationViewLoading.resumeAnimation();
        animationViewLoading.setVisibility(View.VISIBLE);
        layoutMain.setVisibility(View.GONE);
    }

    private void endLoading() {
        animationViewLoading.setVisibility(View.GONE);
        layoutMain.setVisibility(View.VISIBLE);
    }
}
