package com.tat.feri.logofinder.services;

/**
 * Created by tdx on 06/06/2018.
 */

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.HttpUtils;
import com.tat.feri.logofinder.events.AlertStatusEvent;
import com.tat.feri.logofinder.events.LocationCheckEvent;
import com.tat.feri.logofinder.events.LocationDetected;
import com.tat.feri.logofinder.events.ResultListEvent;
import com.tat.feri.logofinder.models.DefaultModel;
import com.tat.feri.logofinder.models.Logo;
import com.tat.feri.logofinder.models.Result;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ResultTracker extends Service  {

    private static final String TAG = ResultTracker.class.getSimpleName();

    private List<Result> results;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);


        return START_STICKY;
    }

    private class CheckResultsCall extends AsyncTask<String, String, List<Result>> {
        @Override
        protected List<Result> doInBackground(String... strings) {
            return Result.all();
        }

        @Override
        protected void onPostExecute(List<Result> results) {
            super.onPostExecute(results);

            if (results != null) {
                Log.e(TAG, "RESULTS FOUND");
                EventBus.getDefault().post(new ResultListEvent(results));
            }
        }
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        EventBus.getDefault().register(this);

        new CheckResultsCall().execute();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AlertStatusEvent event) {
        if (event.isCanceled()) {
            new CheckResultsCall().execute();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationDetected event) {

    }
}