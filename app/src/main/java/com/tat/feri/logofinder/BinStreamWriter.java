package com.tat.feri.logofinder;

/**
 * Created by tdx on 10/01/2019.
 */

import android.util.Log;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class BinStreamWriter
{
    private OutputStream stream;

    private int bitCount = 0;
    private int currentByte = 0;

    private int count = 0;

    /** Creates a OutputStreamBitWriter from an OutputStream */
    public BinStreamWriter (OutputStream s)
    {
        stream = s;
    }

    /**
     * Create a OutputStreamBitWriter
     *
     * @param filename
     *            an absolute or relative filename
     * @exception FileNotFoundException
     *                thrown if the file is not writable
     */
    public BinStreamWriter (String filename) {
        try
        {
            stream = new BufferedOutputStream (new FileOutputStream (filename));
        }
        catch(FileNotFoundException e)
        {
            System.out.println(filename + " does not exist");
        }
    }


    public void writeBit (boolean bit) {
        currentByte <<= 1;
        if (bit) currentByte |= 1;

        Log.e("PARSE", bitCount + ". " + (bit ? 1 : 0) + " WRITE BIT");

        // if buffer is full (8 bits), write out as a single byte
        bitCount++;
        if (bitCount == 8) clearBuffer();

//        currentByte = currentByte << 1 | bit;
//        bitCount++;
//        if (bitCount == 8) {
//            try{stream.write (currentByte);}catch(IOException e){}
//            currentByte = 0;
//            bitCount = 0;
//        }

    }

    private void clearBuffer() {
        if (bitCount == 0) return;
        if (bitCount > 0) currentByte <<= (8 - bitCount);
        try {
            Log.e("PARSE", count++ + ". " + currentByte + " WRITE BYTE");
            stream.write(currentByte);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        bitCount = 0;
        currentByte = 0;
    }

    public void writeBits (int bits, int num) {
        if ((num < 0) || (num > 32))
            throw new IllegalArgumentException ("Number of bits is out of range");

        while (num > 0) {
            // write out byte by byte
            int cbit = Math.min (num, (8 - bitCount));

            currentByte = (currentByte << cbit) | ((bits >>> (num - cbit)) & ((1 << cbit) - 1));

            bitCount += cbit;
            num -= cbit;

            // flush to output
            if (bitCount == 8) {
                try{stream.write (currentByte);}catch(IOException e){}
                currentByte = 0;
                bitCount = 0;
            }
        }
    }

    public void writeByte (byte nextByte) {
        // fast path
        if (bitCount == 0)
            try{stream.write (nextByte);}catch(IOException e){}
        else
            writeBits (nextByte, 8);
    }

    public void writeBytes (byte [] bytes) {
        if (bitCount == 0)
            try{stream.write (bytes);}catch(IOException e){}
        else {
            for (byte b : bytes)
                writeByte (b);
        }
    }

    private byte[] intToBytes(int i) {
        byte[] result = new byte[4];

        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);

        return result;
    }

    public void writeInt (int value) {
        // see http://msdn2.microsoft.com/en-US/library/system.io.binarywriter.write7bitencodedint.aspx
        // about the format. Code taken from Mono's BinaryWriter.
//        do {
//            int high = (value >>> 7) & 0x01ffffff;
//            byte b = (byte)(value & 0x7f);
//
//            if (high != 0) {
//                b = (byte)(b | 0x80);
//            }
//
//            writeByte (b);
//            value = high;
//        } while(value != 0);
//        ByteBuffer b = ByteBuffer.allocate(4);
//        b.order(ByteOrder.LITTLE_ENDIAN); // optional, the initial order of a byte buffer is always BIG_ENDIAN.
//
//
//        b.putInt(value);

        byte[] bArr = intToBytes(value);

        for (int i = 0; i < bArr.length; i++) {
            Log.e("BYTE", i + ". " + bArr[i] + "=" + value);
        }
        Log.e("BYTE",bArr.length + " END BYTE");
        writeBytes(bArr);
    }

    public void flush () {
        while (bitCount > 0)
            writeBit (false);

        try{stream.flush ();}catch(IOException e){}
    }
}