package com.tat.feri.logofinder;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tdx on 08/01/2019.
 */

public class DCT
{

    private Bitmap image;

    private Mat QNTMatrix;
    private Mat DCTMatrix;

    private float quality;
    private int size;

    public DCT(Bitmap image, float quality, int size) {
        this.image = image;
        this.quality = quality;
        this.size = size;

//        Log.e("asd", createDCTMatrix(8).dump());
//        Log.e("asd", createQNTMatrix(8, 50).dump());

        this.QNTMatrix = createQNTMatrix(size, quality);
        this.QNTMatrix = createQNTMatrix(size, quality);
    }

    public DCT(Bitmap image, float quality) {
        this(image, quality, 8);
    }

    private Mat createQNTMatrix(int n, float fact) {
        fact = Math.round(((n*2-1) * fact) / 100);
        Mat mtx = new Mat(n, n, CvType.CV_16U);
        for (int y = 0; y < n; y++) {
            for (int x = 0; x < n; x++) {
                float org = n*2 - (y + x) - 1;
                mtx.put(x, y, org - fact > 0 ? 1 : 0);
            }
        }
        return mtx;
    }

    private Mat quantize(Mat dct) {
        Mat quantized = new Mat(dct.size(), dct.type());
        for (int y = 0; y < dct.height(); y++) {
            for (int x = 0; x < dct.width(); x++) {
                quantized.put(y, x, dct.get(y, x)[0] * QNTMatrix.get(y, x)[0]);
            }
        }
        return quantized;
    }

    private List<Integer> toZigZagArray(Mat dct) {
        List<Integer> zigzag = new ArrayList<>();
        boolean dir = true;
        int y = 0;
        int x = 0;
        while (true) {
            zigzag.add((int)Math.round(dct.get(y, x)[0]));
//            Log.e("zigzag", (int)Math.round(dct.get(y, x)[0]) + "");
            if (y == dct.height() - 1 && x == dct.width() - 1) {
//                Log.e("zigzag", "ZIGZAG END");
                return zigzag;
            }
            if (dir) {
                y -= 1;
                x += 1;
                if (x >= dct.width()) {
                    y += 2;
                    x -= 1;
                    dir = false;
                } else if (y < 0) {
                    y += 1;
                    dir = false;
                }
            } else {
                y += 1;
                x -= 1;
                if (y >= dct.height()) {
                    x += 2;
                    y -= 1;
                    dir = true;
                } else if (x < 0) {
                    x += 1;
                    dir = true;
                }
            }
        }
    }

    private Mat generateDCT(int y, int x, Mat channel) {
        Mat dct = new Mat(size, size, channel.type());
//        Log.e("IDCT", channel.submat(y, y+8, x, x+8).dump());
        Core.dct(channel.submat(y, y+8, x, x+8), dct);
        return dct;
    }

    public byte[] compressImage() {
        Mat img = new Mat();

        List<Mat> channels = new ArrayList<>();


        Bitmap bmp32 = image.copy(Bitmap.Config.RGB_565, true);
        Utils.bitmapToMat(bmp32, img);
        img.convertTo(img, CvType.CV_32F);
        Core.split(img, channels);

        Mat channel = new Mat(img.height(),img.width(),CvType.CV_32F);
        Imgproc.cvtColor(img, channel, Imgproc.COLOR_RGB2GRAY);

        List<Integer> all = new ArrayList<>();

        int index = 0;
//        for (int i = 0; i < channels.size(); i++) {
//            Mat channel = channels.get(i);

//            Log.e("IMAGEINFO", i + "=" + channel.width() + ", " + channel.height() + " CHANNEL SIZE");

            for (int y = 0; y < channel.height(); y += size) {
                for (int x = 0; x < channel.width(); x += size) {

                    Log.e("IMAGEINFO", y + ", " + x + "=" + channel.get(y,x)[0]);

                    Mat dct = generateDCT(y, x, channel);

                    dct = quantize(dct);

                    List<Integer> zigzag = toZigZagArray(dct);
                    all.addAll(zigzag);

                    // HUFFMAN / RLE ENCODING

//                    if (index++ < 10) {
//                        Log.e("DCT", dct.dump());
//                    }

                }
            }
//        }
//        Log.e("IMAGEINFO", channels.size() + " CHANNELS ");

        HuffmanEncode encoder = new HuffmanEncode(all);
        encoder.encode();
        ByteArrayOutputStream stream = encoder.getStream();
        return stream.toByteArray();
    }
}