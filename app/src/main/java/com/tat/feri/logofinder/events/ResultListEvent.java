package com.tat.feri.logofinder.events;

import com.tat.feri.logofinder.models.Logo;
import com.tat.feri.logofinder.models.Result;

import java.util.List;

/**
 * Created by tdx on 06/06/2018.
 */

public class ResultListEvent {
    private List<Result> results;

    public ResultListEvent(List<Result> results) {
        this.results = results;
    }

    public List<Result> getResults() {
        return results;
    }
}
