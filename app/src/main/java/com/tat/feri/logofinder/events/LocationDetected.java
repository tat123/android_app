package com.tat.feri.logofinder.events;

import android.location.Location;

/**
 * Created by tdx on 11/06/2018.
 */

public class LocationDetected {
    private Location location;

    public LocationDetected(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
