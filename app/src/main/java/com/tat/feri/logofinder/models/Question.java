package com.tat.feri.logofinder.models;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tdx on 05/06/2018.
 */

public class Question {

    public static final String identifier = "QuestionID";

    private int ID;
    private String content;
    private Answer[] answers;

    public Question(int ID, String content, Answer[] answers) {
        this.ID = ID;
        this.content = content;
        this.answers = answers;
    }

    public Question(int ID, String content) {
        this.ID = ID;
        this.content = content;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Answer[] getAnswers() {
        return answers;
    }

    public void setAnswers(Answer[] answers) {
        this.answers = answers;
    }

    public static Question find(int ID) {
        return all().get(ID);
    }

    public static List<Question> all() {
        return Arrays.asList(
            new Question(1, "Kdo je to?"),
            new Question(2,"Kdo si ti?"),
            new Question(3, "Kam greš?")
        );
    }
}
