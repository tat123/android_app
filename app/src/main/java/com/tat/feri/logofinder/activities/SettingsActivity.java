package com.tat.feri.logofinder.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaActionSound;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.google.android.cameraview.CameraView;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.DCT;
import com.tat.feri.logofinder.HttpUtils;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.models.DefaultModel;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class SettingsActivity extends AppCompatActivity {

    ApplicationMy app;

    private Toolbar toolbar;

    private FloatingActionButton saveBtn;
    private SeekBar compLvlBar;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        app = (ApplicationMy)getApplication();
        if (!app.isLoggedIn()) {
            Intent intent = new Intent(this.getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        prefs = this.getPreferences(Context.MODE_PRIVATE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Logo Hunter");
        toolbar.inflateMenu(R.menu.actionbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.logout) {
                    app.signOut();
                    Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                return false;
            }
        });

        compLvlBar = (SeekBar)findViewById(R.id.compLvlBar);
        compLvlBar.setProgress(prefs.getInt("COMP_LVL", 50));

        saveBtn = (FloatingActionButton)findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("COMP_LVL", compLvlBar.getProgress());
                editor.commit();

                finish();
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

}
