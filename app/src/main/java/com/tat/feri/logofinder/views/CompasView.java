package com.tat.feri.logofinder.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class CompasView extends View  {
    private int units_of_60 = 15;

    private int indicator;
    private double speed;
    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    private Paint arrowPaint;
    private Paint circlePaint;
    private Paint linePaint;
    private RectF rec=new RectF();;
    private RectF rec2=new RectF();;

    public CompasView(Context context) {
        super(context);
        initClockView();
    }

    public CompasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initClockView();
    }

    public CompasView(Context context, AttributeSet ats, int defaultStyle) {
        super(context, ats, defaultStyle);
        initClockView();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();
        System.out.println(eventX+" "+eventY);
        int mMeasuredWidth = getMeasuredWidth();
        int mMeasuredHeight = getMeasuredHeight();
        int d = Math.min(mMeasuredWidth, mMeasuredHeight);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //speed+=100;
                indicator = (int) ((eventY/d)*100);
                return true;
            case MotionEvent.ACTION_MOVE:
                // path.lineTo(eventX, eventY);
                indicator = (int) ((eventY/d)*100);
                break;
            case MotionEvent.ACTION_UP:
                indicator = (int) ((eventY/d)*100);
                break;
            default:
                return false;
        }
        if (indicator>100) indicator=100;
        if (indicator<1) indicator=1;

        // Schedules a repaint.
        invalidate();
        return true;
    }
    protected void initClockView() {
        speed = 1000;
        setFocusable(false);
        indicator = 50;
        arrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arrowPaint.setColor(Color.WHITE);
        arrowPaint.setStrokeWidth(30);
        arrowPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(Color.WHITE);
        circlePaint.setStrokeWidth(40);
        circlePaint.setStyle(Paint.Style.STROKE);
        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setColor(Color.WHITE);
        linePaint.setStrokeWidth(10);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setPathEffect(new DashPathEffect(new float[] {60,20}, -50));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int mMeasuredWidth = getMeasuredWidth();
        int mMeasuredHeight = getMeasuredHeight();
        int d = Math.min(mMeasuredWidth, mMeasuredHeight);

//        canvas.drawCircle(px, py, radius, circlePaint); //empty circle!
//        canvas.drawCircle(px+radius+20, d*indicator/100, 5, arcPaint); //empty circle!

        d /= 1.5;

        Path path = new Path();
        path.moveTo(Math.round(0.1*d), Math.round(0.2*d));
        path.lineTo(0, Math.round(d*0.2));
        path.lineTo(Math.round(0.1*d), 0);
        path.lineTo(Math.round(0.2*d), Math.round(0.2*d));
        path.lineTo(Math.round(0.1*d), Math.round(0.2*d));
        path.lineTo(Math.round(0.1*d), d);
        path.close();
        path.offset((mMeasuredWidth-Math.round(0.2*d)) / 2, (mMeasuredHeight-d) / 2);

        Path path2 = new Path();
        path2.moveTo(0, Math.round(d/2));
        path2.lineTo(d, Math.round(d/2));
        path2.offset((mMeasuredWidth-Math.round(d)) / 2, (mMeasuredHeight-d) / 2);

        canvas.drawPath(path, arrowPaint);
        canvas.drawPath(path2, linePaint);
        canvas.drawCircle(Math.round(mMeasuredWidth/2), Math.round(mMeasuredHeight/2), d/2 + 60, circlePaint);


//        Path path = new Path();
//        path.moveTo(0, Math.round(0.2*d));
//        path.lineTo(Math.round(0.1*d), 0);
//        path.moveTo(Math.round(0.2*d), Math.round(0.2*d));
//        path.lineTo(Math.round(0.1*d), 0);
//        path.moveTo(Math.round(0.2*d), Math.round(0.2*d));
//        path.lineTo(-1, Math.round(0.2*d));
//        path.moveTo(Math.round(0.1*d), Math.round(0.2*d));
//        path.lineTo(Math.round(0.1*d), d);
//        path.close();
    }

}
