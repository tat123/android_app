package com.tat.feri.logofinder;

/**
 * Created by tdx on 04/06/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tat.feri.logofinder.activities.AdminAddEditLogoActivity;
import com.tat.feri.logofinder.models.Logo;

import java.util.List;

/**
 * Created by tdx on 26/03/2018.
 */

public class AdapterLogos extends RecyclerView.Adapter<AdapterLogos.ViewHolder> {

    List<Logo> items;
    Activity activity;

    public AdapterLogos(List<Logo> items, Activity activity) {
        this.items = items;
        this.activity = activity;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public ImageView iv;

        public ViewHolder(View v) {
            super(v);
            txtHeader = (TextView) v.findViewById(R.id.name);
            txtFooter = (TextView) v.findViewById(R.id.instructions);
            iv = (ImageView)v.findViewById(R.id.icon);
        }
    }

    public void add(Logo logo) {
        items.add(logo);
        notifyItemInserted(items.indexOf(logo));
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_logo_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    private static void startDView(int id, Activity ac) {
        System.out.println("id"+":"+id);
        Intent i = new Intent(ac.getBaseContext(), AdminAddEditLogoActivity.class);
        i.putExtra(Logo.identifier, id);
        ac.startActivity(i);

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Logo logo = items.get(position);

        holder.txtHeader.setText(logo.getName());
//        holder.txtFooter.setText(curCp.getInstructions());
        holder.txtHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdapterLogos.startDView(logo.getID(), activity);
            }
        });
        holder.txtFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdapterLogos.startDView(logo.getID(), activity);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }
}
