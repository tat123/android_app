package com.tat.feri.logofinder.models;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tdx on 05/06/2018.
 */

public class Answer {

    public static final String identifier = "AnswerID";

    private int ID;
    private String answer;
    private int correct;

    public Answer(int ID, String answer, int correct) {
        this.ID = ID;
        this.answer = answer;
        this.correct = correct;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getContent() {
        return answer;
    }

    public void setContent(String content) {
        this.answer = content;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

//    public static Answer find(int ID) {
//        return all()[ID];
//    }


//    public static Answer[] all() {
//        return Arrays.asList(
//            new Answer(1, "Janez", 1),
//            new Answer(2,"Domen", 0),
//            new Answer(3, "SImon", 0),
//            new Answer(3, "Alen", 0)
//        ).toArray();
//    }
}
