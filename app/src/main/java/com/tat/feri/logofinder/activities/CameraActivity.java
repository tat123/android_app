package com.tat.feri.logofinder.activities;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.media.MediaActionSound;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.google.android.cameraview.CameraView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.DCT;
import com.tat.feri.logofinder.HttpUtils;
import com.tat.feri.logofinder.HuffmanEncode;
import com.tat.feri.logofinder.R;
import com.tat.feri.logofinder.events.LocationCheckEvent;
import com.tat.feri.logofinder.events.LocationDetected;
import com.tat.feri.logofinder.models.DefaultModel;
import com.tat.feri.logofinder.models.Logo;
import com.tat.feri.logofinder.services.GPSTracker;
import com.tat.feri.logofinder.services.LogoTracker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class CameraActivity extends AppCompatActivity {

    ApplicationMy app;

    private SharedPreferences prefs;

    private Toolbar toolbar;

    private CameraView cameraView;
    private ImageView previewView;
    private FloatingActionButton cameraBtn;
    private ProgressBar loader;

    private Logo logo;

    private ConstraintLayout layoutMain;
    private LottieAnimationView animationViewLoading;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.e("LOGO", "CV LOADED");
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        app = (ApplicationMy)getApplication();
        if (!app.isLoggedIn()) {
            Intent intent = new Intent(this.getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        layoutMain = (ConstraintLayout) findViewById(R.id.layoutMain);
        animationViewLoading = (LottieAnimationView)findViewById(R.id.animationViewLoading);

        startLoading();

        prefs = this.getPreferences(Context.MODE_PRIVATE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Logo Hunter");
        toolbar.inflateMenu(R.menu.actionbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent;
                switch (item.getItemId()) {
                    case R.id.logout:
                        app.signOut();
                        intent = new Intent(CameraActivity.this, LoginActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.settings:
                        intent = new Intent(CameraActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        return true;
                }

                return false;
            }
        });

        loader = (ProgressBar)findViewById(R.id.loader);
        previewView = (ImageView)findViewById(R.id.previewView);

        cameraView = (CameraView)findViewById(R.id.camera);
        cameraView.addCallback(new CameraView.Callback() {
            @Override
            public void onPictureTaken(CameraView cameraView, byte[] data) {
                super.onPictureTaken(cameraView, data);

                Log.e("APP", "PICTURE TAKEN");

                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                previewView.setImageBitmap(bitmap);
                previewView.setVisibility(View.VISIBLE);

                bitmap = Bitmap.createScaledBitmap(bitmap, 64, 64, true);

                new CompareLogoCall().execute(bitmap);
            }
        });

        cameraBtn = (FloatingActionButton)findViewById(R.id.cameraBtn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("APP", "TAKE PICTURE");

                MediaActionSound sound = new MediaActionSound();
                sound.play(MediaActionSound.SHUTTER_CLICK);

                loader.setVisibility(View.VISIBLE);
                cameraBtn.setVisibility(View.INVISIBLE);

//                cameraView.
                cameraView.takePicture();
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        startLoading();

        startService(new Intent(this, GPSTracker.class));
        startService(new Intent(this, LogoTracker.class));//start service

        if (!OpenCVLoader.initDebug()) {
            Log.d("LOGO", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
        } else {
            Log.d("LOGO", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private class CompareLogoCall extends AsyncTask<Bitmap, Boolean, Boolean> {
        @Override
        protected Boolean doInBackground(Bitmap... images) {
            return processImage(images[0]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(CameraActivity.this, "Logo najden!", Toast.LENGTH_LONG).show();
                Log.e("APP", "LOGOTA SE UJEMATA");
                Intent intent = new Intent(CameraActivity.this, MapsActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(CameraActivity.this, "Sliki se ne ujemata, poskusi ponovno!", Toast.LENGTH_LONG).show();
                Log.e("APP", "LOGOTA SE NE UJEMATA");
            }

            previewView.setVisibility(View.INVISIBLE);
            loader.setVisibility(View.INVISIBLE);
            cameraBtn.setVisibility(View.VISIBLE);
        }
    }

    private boolean processImage(Bitmap image) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

//        DCT dct = new DCT(image, prefs.getInt("COMP_LVL", 50));
//        byte[] bitmapdata = dct.compressImage();
//
//        Log.e("PARSE", getFilesDir().getAbsolutePath());
//        File file = new File(getFilesDir(), "data.txt");
//
//        Log.e("PARSE", file.getAbsolutePath());
//        try {
//            FileOutputStream outputStream = openFileOutput("data.binza", MODE_PRIVATE);
////            if (!file.exists())
////                file.createNewFile();
//            outputStream.write(bitmapdata);
//            outputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();

        try {
            // Connect to the web server endpoint
            URL serverUrl = new URL(HttpUtils.url("logos/Compare"));
            HttpURLConnection urlConnection = (HttpURLConnection) serverUrl.openConnection();
            Log.e("APP", serverUrl.toString());

            String boundaryString = "--BOUNDARY--";

            // Indicate that we want to write to the HTTP request body
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundaryString);

            OutputStream outputStreamToRequestBody = urlConnection.getOutputStream();
            BufferedWriter httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(outputStreamToRequestBody));

            // Include value from the myFileDescription text area in the post data
            httpRequestBodyWriter.write("\n\n--" + boundaryString + "\n");
            httpRequestBodyWriter.write("Content-Disposition: form-data; name=\"userId\"");
            httpRequestBodyWriter.write("\n\n");
            httpRequestBodyWriter.write(app.mAuth.getUid());

            // Include value from the myFileDescription text area in the post data
            httpRequestBodyWriter.write("\n\n--" + boundaryString + "\n");
            httpRequestBodyWriter.write("Content-Disposition: form-data; name=\"logoId\"");
            httpRequestBodyWriter.write("\n\n");
            httpRequestBodyWriter.write("" + logo.getID());

            // Include the section to describe the file
            httpRequestBodyWriter.write("\n--" + boundaryString + "\n");
            httpRequestBodyWriter.write("Content-Disposition: form-data;"
                    + "name=\"image\";"
                    + "filename=\"image.jpg\""
                    + "\nContent-Type: text/plain\n\n");
            httpRequestBodyWriter.flush();

            outputStreamToRequestBody.write(bitmapdata, 0, bitmapdata.length);

            outputStreamToRequestBody.flush();

            // Mark the end of the multipart http request
            httpRequestBodyWriter.write("\n--" + boundaryString + "--\n");
            httpRequestBodyWriter.flush();

            // Close the streams
            outputStreamToRequestBody.close();
            httpRequestBodyWriter.close();

            urlConnection.connect();

            int code = urlConnection.getResponseCode();

            Log.e("LOGO", urlConnection.getResponseMessage());
            if (200 <= code && code <= 299) {
                String response = DefaultModel.readInputStreamToString(urlConnection);
                Log.e("LOGO", response.toString());
                return true;
            }
        } catch (Exception e) { e.printStackTrace(); }

        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationDetected event) {
//        if (mLocation == null) {
//            mLocation = event.getLocation();
//        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LocationCheckEvent event) {
        if (event.hasLogo()) {
            final Logo logo = event.getLogo();
            if (this.logo == null) {
                endLoading();
                this.logo = logo;
            }
        }
    }

    private void startLoading() {
        animationViewLoading.setRepeatCount(LottieDrawable.INFINITE);
        animationViewLoading.resumeAnimation();
        animationViewLoading.setVisibility(View.VISIBLE);
        layoutMain.setVisibility(View.GONE);
    }

    private void endLoading() {
        animationViewLoading.setVisibility(View.GONE);
        layoutMain.setVisibility(View.VISIBLE);
        cameraView.start();
    }


}
