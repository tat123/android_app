package com.tat.feri.logofinder.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.tat.feri.lib.Validator;
import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    ApplicationMy app;

    LoginButton loginButton;

    CallbackManager callbackManager;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private ScrollView loginFormView;
    private TextView titleView;

    private LottieAnimationView animationViewStart, animationViewRepeat, animationViewLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        app = (ApplicationMy) this.getApplication();
        if (app.isLoggedIn()) {
            redirect();
        }

        loginFormView = (ScrollView)findViewById(R.id.login_form);
        titleView = (TextView)findViewById(R.id.titleView);
        animationViewStart = (LottieAnimationView) findViewById(R.id.animationViewStart);
        animationViewRepeat = (LottieAnimationView)findViewById(R.id.animationViewRepeat);
        animationViewLoading = (LottieAnimationView)findViewById(R.id.animationViewLoading);

        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(2500);
                }catch(InterruptedException e) { e.printStackTrace(); }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        titleView.animate().setDuration(400).alpha(1).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                titleView.setVisibility(View.VISIBLE);
                            }
                        });
                        loginFormView.animate().setDuration(400).alpha(1).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                loginFormView.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });

                try {
                    sleep(12000);
                }catch(InterruptedException e) { e.printStackTrace(); }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        animationViewStart.setVisibility(View.GONE);
                        animationViewRepeat.setVisibility(View.VISIBLE);
                        animationViewRepeat.setRepeatCount(LottieDrawable.INFINITE);
                        animationViewRepeat.resumeAnimation();
                    }
                });
            }
        };
        myThread.start();

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        // Register
        Button register = (Button)findViewById(R.id.register_button);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);

            }
        });

        // Facebook

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = new CallbackManager.Factory().create();

        loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoading();
                LoginManager.getInstance().registerCallback(
                    callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            handleFacebookAccessToken(loginResult.getAccessToken());
                        }

                        @Override
                        public void onCancel() {
                            endLoading();
                            Toast.makeText (LoginActivity.this, "FACEBOOK LOGIN CANCEL", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            endLoading();
                            exception.printStackTrace();
                            Toast.makeText (LoginActivity.this, "FACEBOOK LOGIN ERROR", Toast.LENGTH_SHORT).show();

                        }
                    }
                );
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile",  "email"));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        app = (ApplicationMy)getApplication();
        if (app.isLoggedIn()) {
            redirect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        app.mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        app.setUser(app.mAuth.getCurrentUser());
                        redirect();
                    }

                }
            })
            .addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    endLoading();
                    e.printStackTrace();
                }
            });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Validator.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            startLoading();
            app.mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            app.setUser(app.mAuth.getCurrentUser());
                            redirect();
                        }
                    }
                })
                .addOnFailureListener(LoginActivity.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        endLoading();
                        mEmailView.setError(getString(R.string.error_invalid_credentials));
                    }
                });
        }
    }

    private void startLoading() {
        animationViewLoading.setRepeatCount(LottieDrawable.INFINITE);
        animationViewLoading.resumeAnimation();
        animationViewLoading.setVisibility(View.VISIBLE);
        loginFormView.setVisibility(View.GONE);
    }

    private void endLoading() {
        animationViewLoading.setVisibility(View.GONE);
        loginFormView.setVisibility(View.VISIBLE);
    }

    private void redirect() {
        Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
        startActivity(intent);
    }
}

