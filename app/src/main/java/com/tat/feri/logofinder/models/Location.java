package com.tat.feri.logofinder.models;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tdx on 17/05/2018.
 */

public class Location {

    private int ID;
    private Logo logo;
    private String name;
    private double lat;
    private double lng;

    public Location(int ID, Logo logo, String name, double lat, double lng) {
        this.ID = ID;
        this.logo = logo;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public static List<Location> all() {
        return Arrays.asList(
            new Location(1, Logo.all().get(0), "Mercator 1", 46.563494,15.6345356),
            new Location(2, Logo.all().get(1), "Spar 1", 46.5634914,15.6345356),
            new Location(3, Logo.all().get(2), "H&M 1", 46.5569347,15.6485213),
            new Location(4, Logo.all().get(3), "Deichman 1", 46.5569334,15.6332004)
        );
    }
}
