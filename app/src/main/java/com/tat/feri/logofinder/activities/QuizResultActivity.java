package com.tat.feri.logofinder.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tat.feri.logofinder.ApplicationMy;
import com.tat.feri.logofinder.R;

public class QuizResultActivity extends AppCompatActivity {

    private ApplicationMy app;

    private TextView status;
    private Button ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);

        Bundle extras = getIntent().getExtras();

        app = (ApplicationMy)getApplication();

        status = (TextView)findViewById(R.id.status);
        status.setText(extras.getString("status"));

        ok = (Button)findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(QuizResultActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });
    }
}
