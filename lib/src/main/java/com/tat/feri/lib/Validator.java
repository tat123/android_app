package com.tat.feri.lib;

/**
 * Created by tdx on 17/05/2018.
 */

public class Validator {

    public static boolean isEmpty(String text) {
        return text.isEmpty();
    }

    public static boolean isEmailValid(String email) {
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

    public static boolean isPasswordMatch(String p1, String p2) {
        return p1.equals(p2);
    }

    public static boolean isEmailUnique(String email) {
        // TODO: make validation call to API
        return true;
    }
}
