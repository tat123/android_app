package com.tat.feri.lib;

/**
 * Created by tdx on 17/05/2018.
 */

public class Auth {

    public interface RegisterCallback {
        void onRegisterSuccess();
        void onRegisterErrors();
    }

    public static void register(RegisterCallback callback) {
        callback.onRegisterSuccess();
    }
}
